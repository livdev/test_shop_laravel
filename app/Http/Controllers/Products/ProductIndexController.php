<?php

namespace App\Http\Controllers\Products;

use App\Domain\Product\Product;
use App\Models\Filter;
use App\Models\FilterValue;
use App\Models\FilterValueProduct;
use Illuminate\Http\Request;


class ProductIndexController
{


    private function showAll($param){

        if(is_array($param)){
            foreach ($param as $item){
                if($item==0)
                    return false;
            }
            return true;
        }else{
            return ($param==0?false:true);
        }


    }


    public function __invoke(Request $request)
    {

        /**
         * Получаем какая активная страница сейчас
         */
        $pageSelect=($request->input('page')==""?0:$request->input('page'));

        /**
         * Получаем по каким фильтрам нужно вести поиск
         */
        $getParams=$request->all();

        $pagParap=null;
        $iSelectFilter=0;
        foreach ($getParams as $key=>$value){
            if($key!="page"){
                if($this->showAll($value)){
                    $iSelectFilter++;
                    $pagParap[$key]=$value;
                }
            }
        }




        /**
         * Начинаем запрос к БД
         */
        $products = Product::query();
        if($iSelectFilter) {
            if(count($pagParap)){
                foreach ($pagParap as $key=>$value){
                    $products=$products->whereExists(function ($query) use ($value) {
                        $query->select(\DB::raw(1))
                            ->from('filter_value_products');
                        if(is_array($value)){
                            $query->whereIn('filter_value_products.filter_value_id',$value);
                        }else{
                            $query->where('filter_value_products.filter_value_id',$value);
                        }
                        $query->whereColumn('filter_value_products.product_id','products.id');
                    });
                }
            }
        }
        /**
         * Выводим пагнитацию
         */
        $products=$products->paginate(null,["*"],'page',$pageSelect);


        /**
         * Получаем для вывода фильтра
         */
        $filters = Filter::all();
        $filter_values=FilterValue::get();

        /**
         * Выводим предстовление
         */
        return view('products.index', [
            'products' => $products,
            'filters'=>$filters,
            'pagenow'=>$pageSelect,
            'pagParap'=>$pagParap,
            'filter_values'=>$filter_values]);
    }
}
