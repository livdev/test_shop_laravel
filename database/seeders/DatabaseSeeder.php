<?php

namespace Database\Seeders;

use App\Domain\Cart\Actions\AddCartItem;
use App\Domain\Cart\Actions\InitializeCart;
use App\Domain\Coupon\Coupon;
use App\Domain\Customer\Customer;
use App\Domain\Product\Product;
use App\Models\Filter;
use App\Models\FilterValue;
use App\Models\FilterValueProduct;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {


        $countPRoduct=1000;

        $products = Product::factory($countPRoduct)->create();




        $categoryFilter = Filter::create([
            'name' => 'category',
            'type' => 'select_one',
        ]);

        for($i=0;$i<100;$i++){
            FilterValue::create([
                'filter_id' => $categoryFilter->id,
                'value' =>"category ".$i,
            ]);
        }

        $cityFilter = Filter::create([
            'name' => 'city',
            'type' => 'select_many',
        ]);
        for($i=0;$i<30;$i++){
            FilterValue::create([
                'filter_id' => $cityFilter->id,
                'value' =>"city ".$i,
            ]);
        }



        for($i=0;$i<$countPRoduct;$i++){
            $categSelect=FilterValue::where('filter_id',$categoryFilter->id)->inRandomOrder()->first();
            $cityFilterDatas=FilterValue::where('filter_id',$cityFilter->id)->inRandomOrder()->limit(5)->get();


            FilterValueProduct::create([
                'product_id' => $i,
                'filter_value_id' =>$categSelect->id,
            ]);

            foreach ($cityFilterDatas as $cityFilterData){
                FilterValueProduct::create([
                    'product_id' => $i,
                    'filter_value_id' =>$cityFilterData->id,
                ]);
            }

        }


        Coupon::factory()->create();

        /** @var \App\Models\User $user */
        $user = User::factory()->create([
            'email' => 'admin@shop.com',
            'name' => 'Admin',
        ]);

        $customer = Customer::create([
            'name' => $user->name,
            'email' => $user->email,
            'user_id' => $user->id,
            'street' => 'Street',
            'number' => '101',
            'postal' => '2000',
            'city' => 'City',
            'country' => 'Belgium',
        ]);

        $cart = (new InitializeCart)($customer);

        (new AddCartItem)($cart, $products->random(1)[0], 1);
        (new AddCartItem)($cart, $products->random(1)[0], 1);
        (new AddCartItem)($cart, $products->random(1)[0], 1);
    }
}
