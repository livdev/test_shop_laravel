<div class="wrapper_Filter">
    <form>

        @foreach($filters as $filter)
            <div class="filter_item">
                {{$filter->name}}
                @if($filter->type=="select_one")
                    <select   name="{{$filter->name}}" class="js-example-basic-single" >
                        <option value="0" selected>All</option>
                            @foreach($filtervalues->where('filter_id',$filter->id) as $item)
                                <option value="{{$item->id}}"
                                    @if(isset($pagparap[$filter->name]))
                                        @if($item->id==$pagparap[$filter->name])
                                            selected
                                        @endif
                                    @endif
                                >{{$item->value}}</option>
                            @endforeach

                    </select>
                @endif
                @if($filter->type=="select_many")
                    <select multiple  name="{{$filter->name}}[]" class="js-example-basic-single">
                        <option value="0">All</option>
                        @foreach($filtervalues->where('filter_id',$filter->id) as $item)
                            <option value="{{$item->id}}"
                                    @if(isset($pagparap[$filter->name]))
                                        @if(in_array($item->id,$pagparap[$filter->name]))
                                            selected
                                        @endif
                                    @endif
                            >{{$item->value}}</option>
                        @endforeach
                    </select>
                @endif
            </div>
        @endforeach


        <input type="hidden" name="page" value="0">
        <div style="margin-top: 10px;margin-bottom: 10px">
            <input type="submit" style="background: green;color: white;padding: 10px;border-radius: 10px;cursor: pointer" value="filter">
        </div>
    </form>
</div>